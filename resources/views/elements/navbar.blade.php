<div class="navbar">
   <div class="navbar-inner">
        <a href="/">
            <img id="logo" src="{{ URL::to('/') }}/media/logo.svg" alt="Threls Logo">
            <span id="logo-subtitle">Technical Task</span>
        </a>
        <ul class="nav">
            <li class="{{ (Request::is('/')) ? 'active' : '' }}"><a href="/">Home</a></li>
            <li class="{{ (Request::is('calendar')) ? 'active' : '' }}"><a href="/calendar">Calendar</a></li>
        </ul>
   </div>
</div>