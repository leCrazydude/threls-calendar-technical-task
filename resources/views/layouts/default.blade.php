<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Threls | Calendar App</title>
        <meta name="description" content="Threls Calendar App Test">

        <script  src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

        @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    </head>
    
    <body>
        @include('elements.navbar')

        <div id="app">
            <main class="pt-4 px-2 px-md-5 mb-4">
                @yield('content')
            </main>
        </div>

        @include('elements.footer')
    </body>
</html>