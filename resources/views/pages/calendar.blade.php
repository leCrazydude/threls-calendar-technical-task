@extends('layouts.default')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <calendar-component></calendar-component>
        </div>
    </div>

@endsection