@extends('layouts.default')

@section('content')

<div class="row justify-content-center profile-element">
    <div class="col-12 col-md-4 profile-container">
        <div class="profile" style="background:url('{{ URL::to('/') }}/media/profile.jpg')"></div>
        <h1>Maurice Sultana</h1>
        <p>22<sup>nd</sup> January 1994</p>
        <div class="row contact-details">
            <div class="col-12">
                <div class="row detail">
                    <div class="icon col-2" style="background: url('{{ URL::to('/') }}/media/icons/mail.svg')"></div>
                    <div class="link col-10">
                        <a href="mailto:mauricesultana22@gmail.com" target="_blank">mauricesultana22@gmail.com</a>
                    </div>
                </div>
                <div class="row detail">
                    <div class="icon col-2" style="background: url('{{ URL::to('/') }}/media/icons/phone.svg')"></div>
                    <div class="link col-10">
                        <a href="tel:+35679220194" target="_blank">(+356) 79220194</a>
                    </div>
                </div>
                <div class="row detail">
                    <div class="icon col-2" style="background: url('{{ URL::to('/') }}/media/icons/linkedin.svg')"></div>
                    <div class="link col-10">
                        <a href="https://linkedin.com/in/mauricesultana" target="_blank">linkedin.com/in/mauricesultana</a>
                    </div>
                </div>
                <div class="row detail">
                    <div class="icon col-2" style="background: url('{{ URL::to('/') }}/media/icons/location.svg')"></div>
                    <div class="link col-10">
                        <a href="https://www.google.com/maps/place/Gozo/@36.0469086,14.218782,13z/data=!3m1!4b1!4m6!3m5!1s0x130fb4e6eeeaa5b9:0xbfa42f0fe1b720ae!8m2!3d36.0442999!4d14.2512221!16zL20vMDFkNDY2?entry=ttu" target="_blank">Gozo, Malta</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<a class="btn btn-primary link" href="https://drive.google.com/file/d/1mPlDsyF36VZvQwsGl3I-Rrmt-DZ5FJil/view?usp=drive_link" target="_blank">View My Resume</a>
<a class="btn btn-primary link" href="/calendar">Go To Calendar</a>

@endsection