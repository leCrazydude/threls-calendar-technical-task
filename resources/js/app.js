import './bootstrap';
import { createApp } from 'vue';

const app = createApp({});

app.config.devtools = true

import CalendarComponent from './components/CalendarComponent.vue';
app.component('calendar-component', CalendarComponent);

app.mount('#app');