## About Project

A calendar event management system which allows users to add, update and delete events via a form and validates event overlapping.

<br>

## Technologies

Frontend using Vue 3, backend using Laravel

<br>

## Installation
Follow the steps below to install and launch the app on your local environment:

<br>

**1. Clone Project**<br>
Create a new empty directory and clone the project from GitLab using the following command:<br>***git clone https://gitlab.com/leCrazydude/threls-calendar-technical-task***

<br>

**2. Open Project Folder with IDE**<br>
Use preferred editor to locate and open up the cloned repository ***threls-calendar-technical-task***

<br>

**3. Go to Project Directory using Terminal**<br>
Open up a new terminal window and traverse to ***.\threls-calendar-technical-task***

<br>

**4. Install Dependencies**<br>
Install the required dependencies using ***npm install*** or ***yarn install*** and then execute ***composer install*** to download the required vendor resources

<br>

**5. Build App**<br>
Execute ***npm build*** or ***yarn build*** to build the App

<br>

**6. DB Migrate**<br>
Execute ***php artisan migrate*** to setup the local database

<br>

**7. Launch App**<br>
Execute ***php artisan serve*** in the terminal window and open up the localhost url with the specified port