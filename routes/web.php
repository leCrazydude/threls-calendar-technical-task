<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Home;
use App\Http\Controllers\Calendar;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [Home::class, 'index']);

Route::get('/calendar', [Calendar::class, 'index']);
Route::get('/calendar/get-events', [Calendar::class, 'getEvents']);
Route::post('/calendar/add', [Calendar::class, 'add']);
Route::put('/calendar/update/{id}', [Calendar::class, 'update']);
Route::delete('/calendar/delete/{id}', [Calendar::class, 'delete']);