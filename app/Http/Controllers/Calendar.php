<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CalendarEvent;

class Calendar extends Controller
{
    public function index()
    {
        $calendarEvents = CalendarEvent::where('rec_status', 1)->get();

        return view('pages/calendar', ['calendarEvents' => $calendarEvents]);
    }

    public function getEvents()
    {
        $eventList = CalendarEvent::get(['id', 'event_title AS title', 'event_notes AS notes', 'event_from AS start', 'event_to AS end']);

        return response()->json([
            "events" => $eventList
        ]);
    }

    public function add()
    {
        $overlappingCount = $this->checkOverlappingDates(request('event_from'), request('event_to'));
        
        if (!empty($overlappingCount)) {
            return response()->json([
                'message' => 'Event exists in dates selected!',
                'status' => 0
            ], 500);
        }

        $newEvent = CalendarEvent::create([
            'event_title' => request('event_title'),
            'event_notes' => request('event_notes'),
            'event_from' => request('event_from'),
            'event_to' => request('event_to'),
            'rec_status' => 1,
        ]);

        return response()->json([
            'data' => $newEvent,
            'message' => 'Successfully added new event!',
            'status' => 1
        ]);
    }

    public function update()
    {
        if (empty(request('id'))) {
            return response()->json([
                'message' => 'Unable to update event!',
                'status' => 0
            ], 500);
        }

        $overlappingCount = $this->checkOverlappingDates(request('event')['event_from'], request('event')['event_to'], request('id'));
        
        if (!empty($overlappingCount)) {
            return response()->json([
                'message' => 'Event exists in dates selected!',
                'status' => 0
            ], 500);
        }

        $updateEvent = CalendarEvent::where('id', request('id'))
                                    ->update([
                                        'event_title' => request('event')['event_title'],
                                        'event_notes' => request('event')['event_notes'],
                                        'event_from' => request('event')['event_from'],
                                        'event_to' => request('event')['event_to']
                                    ]);
        
        return response()->json([
            'data' => $updateEvent,
            'message' => 'Successfully updated event!',
            'status' => 1
        ]);
    }

    public function delete()
    {
        CalendarEvent::where('id', request('id'))->delete();
        return response()->json([
            'message' => 'Successfully deleted event!',
            'status' => 1
        ]);
    }

    private function checkOverlappingDates($startTime, $endTime, $updateId = null)
    {
        return CalendarEvent::where(function ($query) use ($startTime, $endTime, $updateId) {
            $query->where(function ($query) use ($startTime, $endTime, $updateId) {
                $query->where('event_from', '<=', $startTime)
                      ->where('event_to', '>=', $startTime)
                      ->where('id', '!=', $updateId);
            })
            ->orWhere(function ($query) use ($startTime, $endTime, $updateId) {
                $query->where('event_from', '<=', $endTime)
                      ->where('event_to', '>=', $endTime)
                      ->where('id', '!=', $updateId);
            })
            ->orWhere(function ($query) use ($startTime, $endTime, $updateId) {
                $query->where('event_from', '>=', $startTime)
                      ->where('event_to', '<=', $endTime)
                      ->where('id', '!=', $updateId);
            })
            ->orWhere(function ($query) use ($startTime, $endTime, $updateId) {
                $query->where('event_from', '<=', $startTime)
                      ->where('event_to', '>=', $endTime)
                      ->where('id', '!=', $updateId);
            });
        })->count();
    }
}
